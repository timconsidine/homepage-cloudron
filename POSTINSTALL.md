**POST INSTALL**

There is a peculiarity of the app/node which means that installation must be completed by :
1. restarting the app from the app control panel
2. refresh the screen using the circular icon in the lower right
3. app should then display the default provided backgroun image 

**AUTHENTICATION**

The app has no native user control or authentication and is therefore public in normal deployment.
However when deployed on Cloudron, it uses `proxyAuth` providing a level of authentication for Cloudron users. 
You can check the github issues about authentication (https://github.com/benphelps/homepage/discussions/529) but it's too long-winded IMHO, so this packages uses Cloudron proxyAuth.

**CONFIG**
The 'content' of links to be displayed is provided in `yaml` files in `/app/data/config` directory..
Some may not like this 'manual' (no UI) way of doing things, but I like it.
Note the warning in the docs about taking care to format 'yaml' files correcty.

Other config options can be studied at https://gethomepage.dev/v0.7.0/configs/settings/.

**IMAGES**
See the warning/advice in the app docs https://gethomepage.dev/v0.7.0/configs/settings/ about restarting the app after uploading a local image to use as a background.

NB : the app expects images for the background to be at `/app/data/public/images`
Upload -> edit settings file with image name -> restart app -> refresh using app icon (lower right)
N.B. in the `config/settings.yaml` the file should be set as `/images/....` but the real locatuion is `/app/data/public/images`.
