A modern, fully static, fast, secure fully proxied, highly customizable application dashboard with integrations for over 100 services and translations into multiple languages. Easily configured via YAML files or through docker label discovery.

## FEATURES 

With features like quick search, bookmarks, weather support, a wide range of integrations and widgets, an elegant and modern design, and a focus on performance, Homepage is your ideal start to the day and a handy companion throughout it.

- Fast : The site is statically generated at build time for instant load times.
- Secure : All API requests to backend services are proxied, keeping your API keys hidden. Constantly reviewed for security by the community.
- For Everyone : Images built for AMD64, ARM64, ARMv7, and ARMv6.
- Full i18n : Support for over 40 languages.
- Service & Web Bookmarks : Add custom links to the homepage.
- Docker Integration : Container status and stats. Automatic service discovery via labels.
- Service Integration : Over 100 service integrations, including popular starr and self-hosted apps.
- Information & Utility Widgets : Weather, time, date, search, and more.
- And much more...
