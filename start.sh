#!/bin/bash

set -eu

mkdir -p /app/data/config

mkdir -p /app/data/public

mkdir -p /app/data/public/images

mkdir -p /app/data/images

cp -r /tmp/skeleton/* /app/data/config

cp /tmp/settings.yaml /app/data/config/settings.yaml

cp -r /tmp/public/* /app/data/public 

cp /tmp/background.jpg /app/data/public/images

cp -r /tmp/images/* /app/data/images 

chown -R cloudron:cloudron /app/data

export npm_config_cache=/app/data/.npm-cache

exec /usr/local/bin/gosu cloudron:cloudron npm start
