FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code /app/data /tmp/skeleton /tmp/public /tmp/images

WORKDIR /app/code

RUN apt-get update && apt-get install -y curl

RUN curl -L https://github.com/benphelps/homepage/archive/refs/tags/v0.7.0.tar.gz | tar -zxvf - --strip-components 1 -C /app/code

RUN cp -R src/skeleton/* /tmp/skeleton \
    && cp -R public/* /tmp/public \
    && cp -R images/* /tmp/images

RUN rm -R .dockerignore .gitignore .github .vscode k3d public images
RUN rm Dockerfile Dockerfile-tilt docker-entrypoint.sh kubernetes.md

RUN npm cache clean --force \
    && npm install \
    && npm run build

ENV NODE_ENV=production
ENV HOMEPAGE_CONFIG_DIR=/app/data/config

ADD start.sh xsetup.sh /app/code/
ADD settings.yaml background.jpg /tmp/

RUN ln -s /app/data/public ./public
RUN ln -s /app/data/images ./images

EXPOSE 3000

CMD [ "/app/code/start.sh" ]
